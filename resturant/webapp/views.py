from django.shortcuts import render,redirect
from django.views.generic import TemplateView, ListView,View
from webapp.models import ReservationDetails
from datetime import datetime
from django.core.mail import send_mail
from django.conf import settings
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
import urllib.request
import urllib.parse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.template import loader
from django.utils.html import strip_tags

# Create your views here.


class HomeView(TemplateView):
    def get(self,request):
        template_name = "index.html"
        # new_table_no=TableNo.objects.first()
        # field_object = TableNo._meta.get_field('table_no')
        # field_value = field_object.value_from_object(new_table_no)
        # print('field_value',field_value)
        # if field_value == 0:
        #     print("inside if ")
        #     context={
        #         'field_value':field_value
        #     }
        #     return render(request,template_name,context)
        # else:
        #     print("inside else")
        return render(request,template_name,context=None)
    def post(self,request):
        template_name = "index.html"
        name = request.POST["name"]
        time = request.POST["time"]
        date = request.POST["date"]
        email = request.POST["email"]
        phone = request.POST["number"]
        d = datetime.strptime(date, '%m/%d/%Y')
        format_date = d.strftime('%Y-%m-%d')
        person = request.POST["person"]
        if (person == 'more'):
            person = request.POST["more"]
        id2 = (ReservationDetails.objects.latest('id').id) + 1
        booking_id2 = 'b'+ str(id2)
        create_booking,created = ReservationDetails.objects.get_or_create(name=name,time=time,date=format_date,no_of_person=person,booking_id=booking_id2,email_id=email,contact_no=phone)
        # obj = TableNo.objects.first()
        # field_object = TableNo._meta.get_field('table_no')
        # field_value = field_object.value_from_object(obj)
        # print('field_object','field_value',field_object,field_value)
        # no = field_value - 1
        # table_no_update = TableNo.objects.update(table_no=no)
        id = ReservationDetails.objects.latest('id').id
        booking_id = 'b'+ str(id)
        html_message = loader.render_to_string(
                'bookingmail.html',
            {
                'message': 'Your Reservation for the table is done with booking id' + '  ' + booking_id + '  ' + 'on' + '  ' + create_booking.date + '  ' + 'at' + ' ' + create_booking.time,
                'note': 'Note: Please try to reach the restaurant 15 minutes prior to the booking time.'
            }
        )
        subject = 'Thank you for reserving a table'
        plain_message = strip_tags(html_message)
        send_mail(subject, plain_message, 'noreply@m2softtech.com', [create_booking.email_id], fail_silently=False, html_message=html_message)

        send_mail( 'New Table Reserved',
        'New Reservation for the table is done with booking id' + '  ' + booking_id + '  ' + 'on' + '  ' + create_booking.date + '  ' + 'at' + ' ' + create_booking.time + '  '  + 'by' +' ' +create_booking.name,
        create_booking.email_id,
        ['pmshettar@gmail.com'],
        fail_silently=False,)
        smsMessage = 'Your Reservation for the table is done with booking id ' + booking_id + ' on ' + create_booking.date + ' at ' + create_booking.time + '\r\n Note: Please try to reach the restaurant 15 minutes prior to the booking time.';
        sendSMS('EC28M2FgjL8-v45PpkRDTOu9xxAjdoKRtesSphgFEV', phone, 'SARNGI', smsMessage)
        messages.success(request, 'Your booking has been confirmed and booking id - ' +booking_id+ '. Check you mail for details.')
        return HttpResponseRedirect("/")
        #return render(request,template_name,context=None)

# class BlockTableView(TemplateView):
#     def get(self,request):
#         template_name = "blocktable.html"
#         booking_id = ReservationDetails.objects.all()
#         context={
#             'booking_id':booking_id
#         }
#         return render(request,template_name,context)
#     def post(self,request):
#         template_name = "blocktable.html"
#         booking_id = request.POST["block_table"]
#         blocked_booking_id = ReservationDetails.objects.get(booking_id = booking_id)
#         print('blocked_booking_id',blocked_booking_id.booking_id)
#         blocktable = ReservationDetails.objects.update(block_table=1)
#         return redirect('/')


# class DashboardView(TemplateView,LoginRequiredMixin):
#     login_url = '/login/'
#     redirect_field_name = 'redirect_to'
#     template_name = "dashboard.html"
@login_required
def dashboard(request):
    template_name = "dashboard/index.html"
    bookings = ReservationDetails.objects.all()

    context = {
    'bookings':bookings,
    }
    return render(request,template_name,context)



def searchposts(request):
    if request.method == 'GET':
        query= request.GET.get('q')
        # submitbutton= request.GET.get('submit')
        # print(submitbutton)

        if query is not None:
            lookups= Q(name__icontains=query) | Q(date__icontains=query)|Q(time__icontains=query)|Q(booking_id__icontains=query)|Q(no_of_person__icontains=query)|Q(email_id__icontains=query)

            results= ReservationDetails.objects.filter(lookups).distinct()

            context={'results': results
                     }
            return render(request, 'dashboard/index.html', context)

        else:
            return render(request, 'dashboard/index.html')

    else:
        return render(request, 'dashboard/index.html')

def logout_request(request):
    return redirect('/')



class AboutUs(TemplateView):
    def get(self,request):
        template_name='about.html'
        return render(request,template_name,context=None)

class Contact(TemplateView):
    def get(self,request):
        template_name='contact.html'
        return render(request,template_name,context=None)


class Services(TemplateView):
    def get(self,request):
        template_name='services.html'
        return render(request,template_name,context=None)

class Gallery(TemplateView):
    def get(self,request):
        template_name='gallery.html'
        return render(request,template_name,context=None)

class Codes(TemplateView):
    def get(self,request):
        template_name='codes.html'
        return render(request,template_name,context=None)

class Single(TemplateView):
    def get(self,request):
        template_name='single.html'
        return render(request,template_name,context=None)
def sendSMS(apikey, numbers, sender, message):
    data = urllib.parse.urlencode({'apikey': apikey, 'numbers': numbers, 'message': message, 'sender': sender})
    data = data.encode('utf-8')
    request = urllib.request.Request("https://api.textlocal.in/send/?")
    f = urllib.request.urlopen(request, data)
    fr = f.read()
    return (fr)
