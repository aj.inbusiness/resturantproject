# Generated by Django 3.0.2 on 2020-02-14 08:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0008_auto_20200211_1506'),
    ]

    operations = [
        migrations.AddField(
            model_name='reservationdetails',
            name='contact_no',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
