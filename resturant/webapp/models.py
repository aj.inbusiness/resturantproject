from django.db import models

# Create your models here.


class ReservationDetails(models.Model):
    name = models.CharField(max_length=100)
    auto_date = models.DateTimeField(auto_now_add=True)
    email_id = models.EmailField()
    date = models.DateField()
    time = models.TimeField()
    no_of_person = models.CharField(max_length=100)
    contact_no = models.IntegerField()
    booking_id = models.CharField(max_length=10)

    def __str__(self):
        bookingID = 'b' + str(self.id)
        return bookingID
