from django.contrib import admin
from webapp.models import ReservationDetails

class ReservationAdmin(admin.ModelAdmin):
    class Media:
        css = {
           'all': ('css/jquery-ui.css', 'css/admin-style.css','https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css')
        }
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js',
            'js/jquery-ui.js',
            'js/admin-custom.js',
            'https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js'
        )
admin.site.register(ReservationDetails, ReservationAdmin)
