from django.urls import path
from django.conf.urls import url
from webapp.views import HomeView,dashboard,searchposts,logout_request,AboutUs,Contact,Services,Gallery,Codes,Single
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', HomeView.as_view(),name='home'),
    url(r'^accounts/login/$', auth_views.LoginView.as_view(), {'template_name': 'registration/login.html'}, name='login'),
    url (r'^dashboard/$', dashboard,name='dashboard'),
    url(r'^logout/$', logout_request,name='logout'),
    url(r'^search/$', searchposts,name='search_result'),
    url (r'^aboutus/$', AboutUs.as_view(),name='aboutus'),
    url (r'^contact/$', Contact.as_view(),name='contact'),
    url (r'^services/$', Services.as_view(),name='services'),
    url (r'^gallery/$', Gallery.as_view(),name='gallery'),
    url (r'^codes/$', Codes.as_view(),name='codes'),
    url (r'^single/$', Single.as_view(),name='single'),
]
