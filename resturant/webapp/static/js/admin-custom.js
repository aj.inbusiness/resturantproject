$(document).ready(function() {
    $('.actions').after().append('<br/><br/><label>Start Date</label> <input type="text" name="startdate" id="startdate" value="" >  <label>End Date</label> <input type="text" name="enddate" id="enddate" value="" > <button type="button" class="button search-button" name="search" >Search</button> <button type="button" class="button clear-button" name="Clear" >Clear</button>');
    //$('#startdate, #enddate').datepicker();
    $('#startdate').datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: function(startDate) {
            $('#enddate').datepicker('option', 'minDate', new Date(startDate));
            $('#enddate').val('');
        }
    });
    $('#enddate').datepicker({
        dateFormat: 'yy-mm-dd',
    });
    $('#startdate').val(getUrlParameter('date__gte'));
    $('#enddate').val(getUrlParameter('date__lte'));
    $(document).on('click', '.search-button', function() {
        if ($('#startdate').val() !="" && $('#enddate').val() !="") {
            //sDateObj = new Date($('#startdate').val())
            //sDate = sDateObj.getFullYear() +"-" + (sDateObj.getMonth() + 1 ) + "-" + sDateObj.getDate();
            //eDateObj = new Date($('#enddate').val())
            //eDate = eDateObj.getFullYear() + "-"+ (eDateObj.getMonth() + 1) + "-" +eDateObj.getDate();
            sDate = $('#startdate').val();
            eDate = $('#enddate').val();
            var redirectPath = window.location.pathname + "?date__gte="+sDate+"&date__lte="+eDate;
            window.location.href = redirectPath;
        }
    });
    $(document).on('click', '.clear-button', function() {
        var redirectPath = window.location.pathname;
        window.location.href = redirectPath;
    });
    $("th span").css({"color": "#000 !important"});
    $('#result_list').DataTable( {
        "pagingType": "full_numbers",
        "ordering": false,
        "bFilter": false,
        "bLengthChange": false,
    });
});
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
